var app = angular.module('appLittlebox', ['ngToast', 'ngFileUpload']);

app.controller('appController', function($scope, $http, ngToast, Upload, $timeout, $location) {

  var socket = io($location.protocol() + '://' + $location.host() + ':' + $location.port());

  //Socket to refresh files view when a user creates a folder.

  socket.on('newFolder', function() {

    $scope.refreshFiles($scope.actualPath);

  });

  //Socket to refresh files view when a user upload a file.



  socket.on('upload', function() {

    console.log('Uploaded');
    $scope.refreshFiles($scope.actualPath);

  });

  //Socket to refresh files view when a user deletes a file.


  socket.on('delete', function() {

    console.log('Deleted');
    $scope.refreshFiles($scope.actualPath);

  });

  $scope.download = function(fileName) {
    ngToast.create({
      className: 'warning',
      content: 'Downloading...',
      timeout: 4000
    });
  };


  //Function called by user interface when he/she creates a folder.

  $scope.newFolder = function() {

    var options = {

      params: {
        path: $scope.actualPath,
        name: $scope.nameFolder
      }

    };

    if (options.params.name) {

      $http.post('/api/newFolder', {}, options)
        .then(function(res) {
          $scope.refreshFiles($scope.actualPath);

          //Message shown if creating folder is successful.

          ngToast.create({
            className: 'success',
            content: 'Create Succesful',
            timeout: 3000
          });
        });
    }

  };

  //Function called by user when he/she request files to delete.


  $scope.deleteFiles = function() {

    //Find files or directories which are checked.

    var checkedFiles = _.filter($scope.filesFromServer, function(file) {
      return file.checked;
    });

    //If checkedFiles has content, it will be stored in options.data.

    if (checkedFiles.length > 0) {

      var options = {
        data: checkedFiles,
        headers: {
          'Content-Type': 'application/json'
        }
      };

      $http.delete('api/files', options)
        .then(onSuccess, onError);

      //If delete is successful.

      function onSuccess() {

        ngToast.create({
          className: 'success',
          content: 'Delete Succesful',
          timeout: 3000
        });

      }
      //If delete is not completely succesful.

      function onError() {
        ngToast.create({
          className: 'danger',
          content: 'Some directories are being used, please try later',
          timeout: 3000
        });

      }

    }
  };

  //Function to change checked status for each file which was checked by user.

  $scope.mainCheckboxChanged = function() {
    _.each($scope.filesFromServer, function(file) {
      file.checked = $scope.mainCheckbox;
    });
  };

  //Function to upload files.

  $scope.uploadFiles = function(files) {

    $scope.files = files;
    if (files && files.length) {

      Upload.upload({
          url: 'api/files/upload?path=' + $scope.actualPath,
          method: 'POST',
          arrayKey: '', // default is '[i]'
          data: {
            files: files
          }
        })
        .then(function(response) {
          $timeout(function() {
            $scope.result = response.data;

            $timeout(function() {
              $scope.progress = -1;
            }, 4000);

            //Message shown when upload is succesfull.

            ngToast.create({
              className: 'success',
              content: 'Upload Succesful',
              timeout: 3000
            });

          });
        }, function(response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        }, function(evt) {
          $scope.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    }
  };


  //Function to return to previous path.

  $scope.levelUp = function() {

    if ($scope.actualPath) {

      var newpath = $scope.actualPath + '/..';

      $scope.refreshFiles(newpath);
    }
  };

  //Function to show files by request path.

  $scope.refreshFiles = function(path) {

    var options = {
      params: {
        path: path
      }
    };


    $http.get('/api/files', options)
      .then(function(res) {
        console.log(res);
        $scope.filesFromServer = res.data.files;
        $scope.actualPath = res.data.path;
        socket.emit('currentDir', res.data.path);

      });


  };

  $scope.refreshFiles();

});
