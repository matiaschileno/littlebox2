var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var fs = require('fs');
var _ = require('lodash');
var path = require('path');
var program = require('commander');
var multer = require('multer');
var bodyParser = require('body-parser');
var S = require('string');
var rimraf = require('rimraf');
var mkdirp = require('mkdirp');

var clients = [];
var paths = {};

io.on('connection', function(socket) {
  clients.push(socket);

  socket.on('currentDir', function(path) {

    //Store path in use in object paths.

    if (path != '') {
      paths[socket.id] = path;
    }

  });

  socket.on('disconnect', function() {

    console.log('Got disconnect');

    //Before deleting socket, the path the user was using will be deleted.

    delete paths[socket.id];

    var i = clients.indexOf(socket);
    clients.splice(i, 1);
  });

});



var storage = multer.diskStorage({
  destination: function(req, file, cb) {

    // console.log('the query: ' + JSON.stringify(req.query));
    cb(null, path.join('uploads', req.query.path));
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname)
  }
})

var upload = multer({
  storage: storage
});

var currentPath = null;

var dir = path.join(process.cwd(), 'uploads');

//create uploads folder
mkdirp(dir);

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({
  extended: true
})); // for parsing application/x-www-form-urlencoded
app.use(express.static('src/public')); //app public directory
app.use(express.static(dir)); //app public directory

app.post('/api/files/upload', upload.array('files'), function(req, res) {
  console.log(req.body); // form fields
  console.log(req.files); // form files

  _.each(clients, function(client) {
    client.emit('upload', {
      path: req.files[0].path
    });
  });

  return res.status(204).end();
});

//To add a new folder to /uploads.


app.post('/api/newFolder', function(req, res) {

  var basepath = path.join(dir, req.query.path);

  mkdirp(path.join(basepath, req.query.name), function(err) {
    if (err) {
      console.error(err)
      return res.status(400).end();
    } else {
      _.each(clients, function(client) {
        client.emit('newFolder');
      });
      return res.status(204).end();
    }
  })

});


app.delete('/api/files', function(req, res) {

  //Check if paths is empty.

  if (_.isEmpty(paths)) {

    _.each(req.body, function(file) {

      var filePath = path.join(dir, file.Path);

      console.log('Deleting: ' + filePath);

      if (file.IsDirectory) {
        rimraf.sync(filePath);
      } else {
        fs.unlink(filePath);
      }
    });
    _.each(clients, function(client) {
      client.emit('delete');
    });
    return res.status(204).end();

  } else {

    //Browse all files to delete

    _.each(req.body, function(file) {

      var filePath = path.join(dir, file.Path);

      //Browse all paths in use to compare with the files' paths to delete.

      _.each(paths, function(pathInUse) {

        var dirInUse = path.join(dir, pathInUse);

        if (dirInUse != filePath) {

          console.log('Deleting: ' + filePath);

          if (file.IsDirectory) {
            rimraf.sync(filePath);
          } else {
            fs.unlink(filePath);
          }

          //Broadcast 'delete' socket to all users.

          _.each(clients, function(client) {
            client.emit('delete');
          });
          return res.status(204).end();

        } else {
          console.log('Some directories are being used, please try later');
          return res.status(409).end();
        }

      });

    });

  }

});

//To show files by request path.

app.get('/api/files', function(req, res) {
  var currentDir = dir;
  var query = req.query.path || '';
  if (query)
    currentDir = path.join(dir, query);
  currentPath = currentDir;


  fs.readdir(currentDir, function(err, files) {
    if (err) {
      throw err;
    }
    var data = [];
    files
      .filter(function(file) {
        return true
      })
      .forEach(function(file) {
        try {
          //console.log("processing ", file);
          var isDirectory = fs.statSync(path.join(currentDir, file)).isDirectory();
          if (isDirectory) {
            data.push({
              Name: file,
              IsDirectory: true,
              Path: path.join(query, file)
            });
          } else {
            var ext = path.extname(file);
            if (program.exclude && _.contains(program.exclude, ext)) {
              console.log("Excluding file: ", file);
              return;
            }
            data.push({
              Name: file,
              Ext: ext,
              IsDirectory: false,
              Path: path.join(query, file)
            });
          }

        } catch (e) {
          console.log(e);
        }

      });
    data = _.sortBy(data, function(f) {
      return f.Name
    });

    //Object returnData to send the files and their paths.

    var returnData = {

      files: data,
      path: S(currentDir).chompLeft(dir).s
    };

    res.json(returnData);
  })
});


var server2 = server.listen(process.env.PORT || 3000, function() {
  var host = server2.address().address;
  var port = server2.address().port;


  console.log('App listening at http://%s:%s', host, port);
});
